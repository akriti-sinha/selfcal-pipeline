import stimela

INPUT="input"
MSDIR="msdir"
OUTPUT="output"

ms1 = "EN1_26th_Feb_p1.ms"
ms2 = "EN1_27th_March_p1.ms"
ms3 = "EN1_28th_March_p1.ms"


mslist = [ms1,ms2,ms3]

recipe = stimela.Recipe("First blind image",
                         ms_dir=MSDIR,JOB_TYPE="docker")



recipe.add("cab/wsclean", "First image", {
     "msname": mslist,
     "datacolumn": "DATA",
     "multiscale":True,
     "join-channels": True,
     "channels-out": 4,
     "fit-spectral-pol":2,
     "size": [8192, 8192],
     "scale": "0.5asec",
     "mgain": 0.8,
#     "local-rms": True,
#     "local-rms-window":15, 
     "weighting-rank-filter":3,
     "weighting-rank-filter-size": 32, 
#     "fitsmask":'CDFS_S01-MFS-image.mask1.fits:input',
#     "threshold":20.e-6,
     "auto-mask":7.0,
     "auto-threshold":0.5, 
     "weight":'briggs -1.0',
     "niter": 50000,
#     "no-update-model-required":True,
     "name": "EN1_1_1st_blind",
     
}, input=INPUT, output=OUTPUT, label="first blind")
 
recipe.run()


