import stimela

INPUT="input"
MSDIR="msdir"
OUTPUT="output"

ms1 = "EN1_26th_Feb_p1.ms"
ms2 = "EN1_27th_March_p1.ms"
ms3 = "EN1_28th_March_p1.ms"

mslist = [ms1,ms2,ms3]

ref_ant_1 = "1,5,4,15"
ref_ant_2 = "1,5,4,25"
ref_ant_3 = "2,25,4,5"


recipe = stimela.Recipe("Amp selfcal",
                         ms_dir=MSDIR,JOB_TYPE="docker")

### Self cal parameters ## 

ref_ant = [ref_ant_1,ref_ant_2,ref_ant_3]
uvrange = '>1.5klambda'
soltime = ['8min','6min','4min','2min']  ## solution time interval
ncycles = 4                                  ## the number of phase cal cycles
thresh = [20e-6,15e-6,12e-6,10e-6]     ## clean threshold
sigma = [6,5,5,4] ## The sigma clipping for generating CLEAN mask, I assume you already have a mask created for first cycle of name halo_mask_0.fits, check the last cleanmask task and provide yours.

iteration = [150000,150000,250000] # CLEAN threshold


recipe = stimela.Recipe("Amp selfcal",
                         ms_dir=MSDIR,JOB_TYPE="docker")

for jj in range(ncycles):
#for jj in range(1,4):

    for ii in range(len(mslist)):

        recipe.add("cab/casa_gaincal", "calibrating {} ms".format(ii),{

     "msname": mslist[ii],
     "caltable":'gain_'+str(ii)+'th_ms.'+str(jj),
     "field":'0',
     "refant":ref_ant[ii],
     "gaintype":'G',
     "refantmode":'flex',
     "solint":soltime[jj],
     "minsnr":3,
     "minblperant": 6,
     "uvrange":uvrange,
     "calmode":'ap' 
 }, input=INPUT, output=OUTPUT, label="cal {} ms".format(ii))

        recipe.add("cab/msutils", "plot_gains {} ms".format(ii), {
   
     "command": "plot_gains",
     "ctable": 'gain_'+str(ii)+'th_ms.'+str(jj)+':output',
     "tabtype": "gain",
     "plot_file": 'gains_'+str(ii)+'th_ms_'+str(jj)+'.png',
     "subplot_scale": 4,
     "plot_dpi": 180,
 }, input=INPUT, output=OUTPUT, label="plot gains {} ms".format(ii))




        recipe.add("cab/casa_applycal", "applying {} ms".format(ii),{
     "msname": mslist[ii],
     "gaintable":['gain_'+str(ii)+'th_ms.'+str(jj)+':output'],
     "applymode":'calflagstrict',
     "calwt":False,
 }, input=INPUT, output=OUTPUT, label="apply to {} ms".format(ii))

#        recipe.add("cab/casa_flagdata", "flagging residual amp cal loop {} ms".format(ii),{
#     "msname": mslist[ii],
#     "mode":'tfcrop',
#     "timecutoff":7.0,
#     "freqcutoff":7.0,
#     "usewindowstats":'both',
#     "halfwin":2,
#     "datacolumn": "RESIDUAL",
#     "extendflags":False, 
# }, input=INPUT, output=OUTPUT, label="flag amp cal loop {} ms".format(ii))
    #recipe.run("flag {} ms".format(ii))

        recipe.add('cab/msutils', "Copy corr column to data of {} ms".format(ii), {
          "command" : 'copycol',
          "msname"  : mslist[ii],
          "fromcol" : 'CORRECTED_DATA',
          "tocol"   : 'DATA',
}, input=INPUT, output=OUTPUT, label="copy {} ms".format(ii))



    recipe.add("cab/wsclean", "Image after selfcal_{}".format(jj), {
     "msname": mslist,
     "datacolumn": "CORRECTED_DATA",
     "multiscale":True,
     "join-channels": True,
     "channels-out": 2,
     "fit-spectral-pol":2,
     "size": [8192, 8192],
     "scale": "1.5asec",
     "mgain": 0.8,
     "weighting-rank-filter":3,
     "weighting-rank-filter-size": 32, 
     "fitsmask":'EN1_1_mask_'+str(jj)+'.fits:output',
     "threshold":thresh[jj],
     "weight":'briggs -1',
     "niter": iteration[jj],
     "name": "EN1_1_masked_amp_"+str(jj),
     
}, input=INPUT, output=OUTPUT, label="mask image")
 
 
    recipe.add("cab/cleanmask", "mask_{}".format(jj), {
     "image":"EN1_1_masked_amp_"+str(jj)+'-MFS-image.fits:output',
     "output": 'EN1_1_mask_amp_'+str(jj+1)+'.fits',
     "sigma":sigma[jj],
     "boxes": 16,
     "tolerance":0.5,
     "no-negative":True,
     "overlap":0.3,
}, input=INPUT, output=OUTPUT, label="mask creation {}".format(jj))
 
    recipe.run()
    

